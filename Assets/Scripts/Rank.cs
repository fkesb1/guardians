﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using UnityEngine;
using LitJson;
using System;
using UnityEngine.UI;

public class Rank : MonoBehaviour
{
    public InputField teamName;
    public EnemySpawner em;
    public GameObject registerPanel;
    public Text noTeamnameErrorText;
    public Text duplicateTeamnameErrorText;
    public Canvas GameOverCanvas;

    public Text MaxWaveText1;
    public Text MaxWaveText2;
    public Text MaxWaveText3;
    public Text TeamNameText1;
    public Text TeamNameText2;
    public Text TeamNameText3;

    // private class rank(){
    //     public Text MaxWaveText;
    //     public Text TeamNameText;
    //
    //     public void setMaxWave(int a){
    //       this.MaxWaveText = a;
    //     }
    //     public void setTeamNameText(String a){
    //       this.TeamNameText = a;
    //     }
    //     public void getMaxWave(){
    //       return this.MaxWaveText;
    //     }
    //     public void getTeamNameText(){
    //       return this.TeamNameText;
    //     }
    // }



    public void Register()
    {
        if (teamName.text.Equals(""))
        {
            if (duplicateTeamnameErrorText.enabled)
                duplicateTeamnameErrorText.enabled = false;
            noTeamnameErrorText.enabled = true;
            return;
        }
        int wave = em.wave;
        upRank(teamName.text, wave);
    }

    public void upRank(string teamname, int maxwave)
    {
        HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://45.76.207.225:1234/voxel/set?teamname=" + teamname + "&f&maxwave=" + maxwave);
        myHttpWebRequest.Timeout = 5000;
        try
        {
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            if (myHttpWebResponse.StatusCode == HttpStatusCode.OK)
            {
                Debug.Log("success");
                registerPanel.SetActive(false);
            }
            else if (myHttpWebResponse.StatusCode == HttpStatusCode.BadRequest)
            {
                Debug.Log("other name go");
            }
            else
            {
                Debug.Log("unknown error");
            }
            myHttpWebResponse.Close();
        }
        catch (Exception)
        {
            Debug.Log("asuccess");
            if (noTeamnameErrorText.enabled)
                noTeamnameErrorText.enabled = false;
            duplicateTeamnameErrorText.enabled = true;
        }

    }
    public void getRank()
    {
        HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://45.76.207.225:1234/voxel/rank");
        myHttpWebRequest.Timeout = 5000;
        try
        {
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            if (myHttpWebResponse.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = myHttpWebResponse.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                string data = readStream.ReadToEnd();
                JsonData getData = JsonMapper.ToObject(data);
                for (int i = 0; i < getData.Count; i++)
                {
                    // ranker[i].setMaxWave(getData[i]["maxwave"]);
                    // ranker[i].setTeamNameText(getData[i]["teamname"]);
                    TeamNameText1.text = (string)getData[0]["teamname"];
                    TeamNameText2.text = (string)getData[1]["teamname"];
                    TeamNameText3.text = (string)getData[2]["teamname"];

                    if ((int)getData[0]["maxwave"] == 11)
                        MaxWaveText1.text = "CLEAR";
                    else
                        MaxWaveText1.text = "Wave " + (int)getData[0]["maxwave"];

                    if ((int)getData[1]["maxwave"] == 11)
                        MaxWaveText2.text = "CLEAR";
                    else
                        MaxWaveText2.text = "Wave " + (int)getData[1]["maxwave"];

                    if ((int)getData[2]["maxwave"] == 11)
                        MaxWaveText3.text = "CLEAR";
                    else
                        MaxWaveText3.text = "Wave " + (int)getData[2]["maxwave"];
                }
            }
            else
            {
                Debug.Log("unknown error");
            }
            myHttpWebResponse.Close();
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
    }
}