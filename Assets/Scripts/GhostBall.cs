﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GhostBall : MonoBehaviour {

    private int damage = 10;

    void OnTriggerEnter(Collider col)
    {
        if(col.transform.CompareTag("Castle"))
        {
            col.GetComponent<CastleHp>().GetDamage(damage);
            Destroy(gameObject);
        }

    }
}
