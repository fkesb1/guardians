﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MonGhost : NetworkBehaviour {

    public GameObject fireballPrefab;
    public GameObject firepos;
    public GameObject energyParticle;

    private AudioSource audio;
    public AudioClip atkSound;

    private Animator anim;
    private float speed = 17.0f;

    [SyncVar]
    public bool isDead = false;

    void Start()
    {
        energyParticle.SetActive(false);
        audio = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (!isDead)
        {
            if (transform.position.z > 140)
            {
                if (anim.GetBool("isAttack").Equals(true))
                    anim.SetBool("isAttack", false);
                transform.Translate(Vector3.forward * speed * Time.deltaTime);
                energyParticle.SetActive(false);
            }
            else
            {
                if (anim.GetBool("isAttack").Equals(false))
                    anim.SetBool("isAttack", true);
            }
        }
        else
        {
            energyParticle.SetActive(false);
        }
    }

    void GatherEnergy()
    {
        energyParticle.SetActive(true);
    }

    void ShootFire()
    {
        energyParticle.SetActive(false);
        audio.PlayOneShot(atkSound);
        if (isServer)
        {
            GameObject fire = (GameObject)Instantiate(fireballPrefab, firepos.transform.position, Quaternion.identity);
            fire.transform.localScale = new Vector3(0, 0, 0);
            fire.GetComponent<Rigidbody>().velocity = -fire.transform.forward * 30;

            NetworkServer.Spawn(fire);
        }
    }
}
