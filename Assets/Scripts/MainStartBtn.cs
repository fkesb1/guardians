﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainStartBtn : MonoBehaviour {

	public Canvas loadingCan;

    void Start()
    {
		NetworkManager_Custom net = GameObject.Find ("NetworkManager").GetComponent<NetworkManager_Custom>();
        GetComponent<Button>().onClick.AddListener(net.JoinGame);
		net.loadingCanvas = loadingCan;
    }
}
