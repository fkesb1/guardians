﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Text;

public class Player_ID : NetworkBehaviour {

	[SyncVar]
    private string playerUniqueIdentity;
	private NetworkInstanceId playerNetID;
	public Transform myTransform;

	public override void OnStartLocalPlayer ()
	{
		GetNetIdentity();
		SetIdentity();
	}
    
	void Awake () 
	{
		myTransform = transform;
	}
    
    void Update()
    {
        if (myTransform.name.Equals("") || myTransform.name.Equals("Player(Clone)"))
        {
            SetIdentity();
        }
    }

    [Client]
	void GetNetIdentity()
	{
		playerNetID = GetComponent<NetworkIdentity>().netId;
		CmdTellServerMyIdentity(MakeUniqueIdentity());
	}
	
	void SetIdentity()
	{
		if(!isLocalPlayer)
		{
			myTransform.name = playerUniqueIdentity;
		}
		else
		{
			myTransform.name = MakeUniqueIdentity();
		}
	}

	string MakeUniqueIdentity()
	{
        StringBuilder sb = new StringBuilder();
        sb.Length = 0;
        sb.Append("Player ");
        sb.Append(playerNetID.ToString());
		//string uniqueName = "Player " + playerNetID.ToString();
        return sb.ToString();
	}

	[Command]
	void CmdTellServerMyIdentity(string name)
	{
		playerUniqueIdentity = name;
	}


}
