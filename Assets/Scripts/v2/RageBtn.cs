﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RageBtn : MonoBehaviour {

    public string playerid;

    private Character player;

    public GameObject launcher;

 
    void Start()
    {
        StartCoroutine("SetPlayer", 1.0f);
    }

    IEnumerator SetPlayer(float wt)
    {
        yield return new WaitForSeconds(wt);
        player = GameObject.Find("Player " + playerid).GetComponent<Character>();
    }
    
    public void EnterRage()
    {
        player.CmdRage();
    }
}
