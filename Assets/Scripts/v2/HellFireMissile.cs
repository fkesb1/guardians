﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class HellFireMissile : NetworkBehaviour {
    public GameObject missile;
    public GameObject effect;
    public AudioClip launchSound;
    private AudioSource aus;
    private int x;
    private int y;

    private int cnt = 0;
    private void Start()
    {
        aus = GetComponent<AudioSource>();
    }
    public void Fire()
    {
        if (!isServer) return;
        GameObject missileobj = (GameObject)Instantiate(missile, transform.position, Quaternion.Euler(75, 0, 0));
        aus.PlayOneShot(launchSound);
        NetworkServer.Spawn(missileobj);
        cnt++;
        Invoke("a", 0.3f);
    }
    public void a()
    {
        x = Random.Range(17, 50);
        y = Random.Range(-15, 15);
        GameObject missileobj = (GameObject)Instantiate(missile, transform.position, Quaternion.Euler(x, y, 0));
        aus.PlayOneShot(launchSound);
        NetworkServer.Spawn(missileobj);
        cnt++;
        if (cnt.Equals(20))
        {
            cnt = 0;
        }
        else
        {
            Invoke("a", 0.3f);
        }
    }
}
