﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Airplane : MonoBehaviour {

    private Vector3 LeftLocation;
    private Vector3 RightLocation;

    private GameObject[] player_list;

    private AudioSource aus;
    public AudioClip flySound;
    public AudioClip loadSound;


    void Start()
    {
        aus = GetComponent<AudioSource>();
        LeftLocation = new Vector3(54.6f, 56.11889f, 79.4f);
        RightLocation = new Vector3(135.5f, 56.11889f, 79.4f);
        sort();
    }

    private void sort()
    {
        player_list = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < player_list.Length; i++)
        {
            if (i % 2 == 1)
            {
                player_list[i].transform.position = LeftLocation;
            }
            else
            {
                player_list[i].transform.position = RightLocation;
            }
        }
    }

    private void kill(int loc)
    {
        for (int i = 0; i < player_list.Length; i++)
        {
            if (i % 2 == loc) {
                Destroy(player_list[i]);
                PlayLoadSound();
            }
        }
    }

    private void PlayFlySound()
    {
        aus.PlayOneShot(flySound);
    }

    private void PlayLoadSound()
    {
        aus.PlayOneShot(loadSound);
    }
}
