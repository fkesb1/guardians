﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class CheatConsole : MonoBehaviour {

    public InputField input;
    public Button btn;
    void Update()
    { 
        if (Input.GetKeyDown("`"))
        {
            EventSystem.current.SetSelectedGameObject(input.gameObject, null);
        }
        if (Input.GetKeyDown("return"))
        {
            getCommand(input.text);
        }
    }
    public void btnClicked()
    { //if ui button clicked(for mobile)
        getCommand(input.text);
    }
    private void getCommand(string args)
    {
        string[] tmp = args.Split(' ');
        if (tmp[0] == "/wave")
        { //ex) /wave 6
            int wave = int.Parse(tmp[1]);
            GameObject.Find("Spawner_Monster").GetComponent<EnemySpawner>().CheatWave(wave);
        }
        else if (tmp[0] == "/money")
        { //ex) /money 10000
            int money = int.Parse(tmp[1]);
            GameObject.Find("TeamInfo").GetComponent<PlayerInfo>().RpcMoneyCheat(money);
        }
    }
}
