﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseCanvas : MonoBehaviour {
    
    void Update()
    {
        if(Input.GetKey(KeyCode.Escape))
        {
            GetComponent<Canvas>().enabled = false;
        }
    }

}
