﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class MissileLaunch : NetworkBehaviour
{
    public Text cooltimeText;
    public GameObject missile;

    private AudioSource aus;
    public AudioClip launchSound;

    private int cnt = 0;

    // 위아래 기울기 17 ~ 50
    private float x;
    // 양옆 기울기 -15 ~ 15
    private float y;

    // 쿨타임
    private int coolTime = 120;
    [SyncVar]
    private int timer;
    [SyncVar]
    private bool isCooltime = false;

    void Start()
    {
        aus = GetComponent<AudioSource>();
        timer = coolTime;
        cooltimeText.text = null;
    }
    
    public void LaunchMissile()
    {
        if (isCooltime) return;
        isCooltime = true;
        Fire();
        StartCoroutine("CoolTime", 0f);
    }

    void Fire()
    {
        GameObject missileobj = (GameObject)Instantiate(missile, transform.position, Quaternion.Euler(75, 0, 0));
        aus.PlayOneShot(launchSound);
        NetworkServer.Spawn(missileobj);
        cnt++;
        Invoke("Firing", 0.3f);
    }

    void Firing()
    {
        x = Random.Range(17, 50);
        y = Random.Range(-15, 15);
        GameObject missileobj = (GameObject)Instantiate(missile, transform.position, Quaternion.Euler(x, y, 0));
        aus.PlayOneShot(launchSound);
        NetworkServer.Spawn(missileobj);
        cnt++;
        if (cnt.Equals(20))
        {
            cnt = 0;
        }
        else
        {
            Invoke("Firing", 0.3f);
        }
    }

    IEnumerator CoolTime(int wt)
    {
        if (isCooltime)
        {
            //isCooltime = true;
            //RpcCooltimeBtn();
            //yield return new WaitForSeconds(wt);
            //isCooltime = false;
            //RpcReadyBtn();
            yield return new WaitForSeconds(wt);

            RpcCooltimeBtn(timer.ToString());
            if (timer.Equals(0))
            {
                isCooltime = false;
                RpcReadyBtn();
                timer = coolTime;
            }

            timer--;
            StartCoroutine("CoolTime", 1.0f);
        }
    }
    
    [ClientRpc]
    void RpcCooltimeBtn(string time)
    {
        cooltimeText.text = time;
    }
    
    [ClientRpc]
    void RpcReadyBtn()
    {
        cooltimeText.text = null;
    }
}
