﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class FireBtn : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public string playerid;
    public PlayerInfo teamInfo;

    private Character player;

    private bool isFire = false;


    private bool isCooltime = false;

    void Start()
    {
        StartCoroutine("SetPlayer", 1.0f);
    }

    IEnumerator SetPlayer(float wt)
    {
        yield return new WaitForSeconds(wt);
        player = GameObject.Find("Player " + playerid).GetComponent<Character>();
        userInfo.playerid = "Player " + playerid;
    }

    IEnumerator Fire(float wt)
    {
        if (!isFire) yield break;
        isCooltime = true;
        player.CmdFire(teamInfo.damage);
        player.ShootingSound();
        yield return new WaitForSeconds(wt);
        isCooltime = false;
        StartCoroutine("Fire", teamInfo.rpm);
    }

    public void OnPointerDown(PointerEventData data)
    {
        shoot();
    }

    public void OnPointerUp(PointerEventData data)
    {
        stopshoot();
    }
    public void shoot()
    {
        isFire = true;
        if (!isCooltime)
            StartCoroutine("Fire", teamInfo.rpm);
    }
    public void stopshoot()
    {
        isFire = false;
        player.anim.SetBool("isShoot", false);
    }
}