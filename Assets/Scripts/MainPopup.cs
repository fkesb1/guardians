﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainPopup : MonoBehaviour {
    
    public Canvas exitCanvas;

    void Update()
    {
        if(Input.GetKey(KeyCode.Escape))
        {
            exitCanvas.enabled = true;
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ExitCancle()
    {
        exitCanvas.enabled = false;
    }
}
