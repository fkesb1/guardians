﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MonBoss : NetworkBehaviour {

    private CastleHp castlehp;

    private AudioSource aus;
    public AudioClip atkSound;
    public AudioClip stepSound;

    private Animator anim;
    private float speed = 15;
    private int attackDamage = 100;
    private int dashDamage = 400;

    private EnemyHP hp;

    public GameObject dashAura;

    private Vector3 castlePosition;

    private bool isDash = false;

    private float startTime;
    private float lerpLength;

    [SyncVar]
    public bool isDead = false;

    private int state = 1;
    void Start()
    {
        castlehp = GameObject.Find("Castle_Final").GetComponent<CastleHp>();

        aus = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        hp = GetComponent<EnemyHP>();
        dashAura.SetActive(false);
        castlePosition = new Vector3(99, 0.44f, 80);
    }

    void Update()
    {
        if (!isDead)
            Pattern();
    }

    void Pattern()
    {
        if (isDash)
        {
            float lerpDist = (Time.time - startTime) * 100;
            float go = lerpDist / lerpLength;
            transform.position = Vector3.Lerp(transform.position, castlePosition, go);
            if(transform.position.z < 81)
            {
                Action_StopDash();
            }
        }
        // 접근
        if (transform.position.z > 295)
        {
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }
        else
        {
            anim.SetBool("isDash", true);
        }

        if(transform.position.z < 81)
        {
            if(anim.GetBool("isDash").Equals(true))
                anim.SetBool("isDash", false);
            anim.SetBool("isAttack", true);
        }
    }

    void Action_GatherEnergy()
    {
        dashAura.SetActive(true);
    }

    void Action_Dash()
    {
        isDash = true;
        dashAura.SetActive(false);
        startTime = Time.time;
        lerpLength = Vector3.Distance(transform.position, castlePosition);
    }

    void Action_StopDash()
    {
        isDash = false;
    }

    void Action_Step()
    {
        aus.PlayOneShot(stepSound);
    }

    void DamageCastleWithAttack()
    {
        aus.PlayOneShot(atkSound);
        castlehp.GetDamage(attackDamage);
    }

    void DamageCastleWithDashAttack()
    {
        aus.PlayOneShot(atkSound);
        castlehp.GetDamage(dashDamage);
    }
}
