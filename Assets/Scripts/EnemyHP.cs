﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class EnemyHP : NetworkBehaviour {

    private Animator anim;

    public Slider hpSlider;

    private static int maxHp;

    private int wave;
    private int strengthenEnemy;
	private int balanceDegree = 50;
	private int balanceMoney = 100;

    [SyncVar]
    public int hp;

    private int whoami; // Zombie = 1, Ghost = 2, Skel = 3, Boss = 4

    private AudioSource audio;
    public AudioClip hitSound;
    private Slider volumeSlider;
    private Toggle volumeToggle;

    private PlayerInfo teamInfo;

    private Slider bossHpSlider;
    private Text bossHpText;

    private bool isGameOver = false;

    void Start()
    {
		if (isServer)
		{
			if (NetworkServer.connections.Count.Equals (1))
			{
				balanceDegree = 20;
				balanceMoney = 150;
			}
		}
        audio = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        teamInfo = GameObject.Find("TeamInfo").GetComponent<PlayerInfo>();
        wave = GameObject.Find("Spawner_Monster").GetComponent<EnemySpawner>().wave;
		strengthenEnemy = (wave - 1) * balanceDegree;

        if (transform.name.Equals("Monster_Zombie(Clone)"))
        {
            maxHp = 200 + strengthenEnemy;
            whoami = 1;
            hp = maxHp;
            hpSlider.maxValue = maxHp;
            hpSlider.value = hp;
        }
        else if (transform.name.Equals("Monster_Ghost(Clone)"))
        {
            maxHp = 70 + strengthenEnemy;
            whoami = 2;
            hp = maxHp;
            hpSlider.maxValue = maxHp;
            hpSlider.value = hp;
        }
        else if (transform.name.Equals("Monster_Skeleton(Clone)"))
        {
            maxHp = 100 + strengthenEnemy;
            whoami = 3;
            hp = maxHp;
            hpSlider.maxValue = maxHp;
            hpSlider.value = hp;
        }
        else if (transform.name.Equals("Monster_Boss(Clone)"))
        {
            bossHpSlider = GameObject.Find("BossHp").GetComponent<Slider>();
            bossHpText = GameObject.Find("Text_BossHp").GetComponent<Text>();
            maxHp = 10000;
            whoami = 4;
            hp = maxHp;
            bossHpSlider.maxValue = maxHp;
            bossHpSlider.value = hp;
            bossHpText.text = hp.ToString();
        }
    }

    public void GetDamage(int damage)
    {
        audio.PlayOneShot(hitSound);
        if (isServer)
        {

            hp -= damage;
            RpcHpSlider(damage);

            if (hp <= 0)
            {
                GetComponent<BoxCollider>().enabled = false;
                anim.SetBool("isDeath", true);
				RpcUpdateMoney(balanceMoney);

                if (whoami.Equals(1))
                {
                    GetComponent<MonZombie>().isDead = true;
                    Destroy(gameObject, 2.0f);
                }
                else if (whoami.Equals(2))
                {
                    GetComponent<MonGhost>().isDead = true;
                    Destroy(gameObject, 2.0f);
                }
                else if (whoami.Equals(3))
                {
                    GetComponent<MonSkeleton>().isDead = true;
                    Destroy(gameObject, 2.0f);
                }
                else if (whoami.Equals(4))
                {
                    GetComponent<MonBoss>().isDead = true;
                    GetComponent<AudioSource>().Play();
                    Destroy(gameObject, 4.0f);
                    GameObject.Find("Spawner_Monster").GetComponent<EnemySpawner>().RpcVictory();
                }
            }
        }
    }

    [ClientRpc]
    void RpcUpdateMoney(int money)
    {
        teamInfo.money += money;
    }

    [ClientRpc]
    void RpcHpSlider(int damage)
    {
        if (whoami.Equals(4))
        {
            bossHpSlider.value -= damage;
            bossHpText.text = bossHpSlider.value.ToString();
        }
        else
            hpSlider.value -= damage;
    }

    public void GameOver()
    {
        if(GetComponent<MonSkeleton>() != null)
        {
            GetComponent<MonSkeleton>().enabled = false;
        }
        if (GetComponent<MonGhost>() != null)
        {
            GetComponent<MonGhost>().energyParticle.SetActive(false);
            GetComponent<MonGhost>().enabled = false;
        }
        if (GetComponent<MonZombie>() != null)
        {
            GetComponent<MonZombie>().enabled = false;
        }
        if (GetComponent<MonBoss>() != null)
        {
            GetComponent<MonBoss>().enabled = false;
        }
        anim.SetBool("isAttack", false);
        isGameOver = true;
    }

    void Update()
    {
        if (!isGameOver) return;

        transform.Translate(Vector3.forward * 20 * Time.deltaTime);
    }
}
