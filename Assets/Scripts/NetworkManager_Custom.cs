﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System;
using UnityEngine.SceneManagement;

public class NetworkManager_Custom : NetworkManager
{
    public Canvas loadingCanvas;

    private bool isError = false;
    private Socket s;
    private IPEndPoint ipepW;
    private EndPoint remoteEpW;
    private string ipAddress;

    public void StartupHost()
    {
        if (!NetworkClient.active && !NetworkServer.active)
        {
            if (createRoom() == 0)
            {
                loadingCanvas.enabled = true;
                SetPort();
                NetworkManager.singleton.StartHost();
            }
            else
            {
                Debug.Log("error to make room");
                return;
            }
        }
    }

    public void JoinGame()
    {
        if (!NetworkClient.active && !NetworkServer.active)
        {
            SetIPAddress(1);
            loadingCanvas.enabled = true;
            if (ipAddress == null)
            {
                SetPort();
                NetworkManager.singleton.StartHost();
            }
            if (ipAddress.Length.Equals(0))
            {
                StartupHost();
                return;
            }
            try
            {
                NetworkManager.singleton.networkAddress = ipAddress;
                SetPort();
                NetworkManager.singleton.StartClient();
            }
            catch (Exception)
            {
                Debug.Log("cannot connect(goto make room)");
                ghostRoom(ipAddress);
            }

        }
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        ghostRoom(ipAddress);
    }
    private int createRoom()
    {
        Packet p = new Packet((int)PacketType.OP_CreateRoom, false, Network.player.ipAddress, "");
        string data = JsonUtility.ToJson(p);
        byte[] buffer = Encoding.UTF8.GetBytes(data);
        byte[] buffer2 = new byte[1024];
        s.SendTimeout = 4000;
        s.ReceiveTimeout = 3000;
        try
        {
            s.SendTo(buffer, remoteEpW);
            s.ReceiveFrom(buffer2, ref remoteEpW);
            string data2 = Encoding.UTF8.GetString(buffer2);
            Packet p2 = JsonUtility.FromJson<Packet>(data2);
            if (p2.packetType == (int)PacketType.OP_CreateRoomReply)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }
        catch (Exception)
        {
            return -1;
        }
    }
    void SetIPAddress(int op)
    {
        Packet p = new Packet((int)PacketType.OP_FindRoom, false, Network.player.ipAddress, "");
        string data = JsonUtility.ToJson(p);
        byte[] buffer = Encoding.UTF8.GetBytes(data);
        byte[] buffer2 = new byte[1024];
        s.SendTimeout = 4000;
        s.ReceiveTimeout = 3000;
        try
        {
            s.SendTo(buffer, remoteEpW);
            s.ReceiveFrom(buffer2, ref remoteEpW);
            string data2 = Encoding.UTF8.GetString(buffer2);
            Packet p2 = JsonUtility.FromJson<Packet>(data2);
            ipAddress = p2.body;
        }
        catch
        {
            if (op == 2)
            {
                ipAddress = null;
                return;
            }
            SetIPAddress(2);
        }
    }
    public void sendToServer(Packet p)
    {
        byte[] buffer = new byte[1024];
        byte[] buffer2 = new byte[1024];

        //buffer = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(p));
        //s.SendTo(buffer, remoteEpW);
        //s.ReceiveFrom(buffer2, ref remoteEpW);
    }
    private void Start()
    {
        s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        //ipepW = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 51000);
        ipepW = new IPEndPoint(IPAddress.Parse("45.77.24.189"), 51000);
        remoteEpW = (EndPoint)ipepW;
    }
    void SetPort()
    {
        NetworkManager.singleton.networkPort = 7777;
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if(scene.name.Equals("StartScene"))
        {
            //StartCoroutine(SetupMenuSceneButtons());
        }
    }

    public void SetupMenuSceneButtons()
    {
        GameObject.Find("Btn_Play").GetComponent<Button>().onClick.RemoveAllListeners();
        GameObject.Find("Btn_Play").GetComponent<Button>().onClick.AddListener(JoinGame);
        loadingCanvas = GameObject.Find("Canvas_Loading").GetComponent<Canvas>();
    }

    void SetupOtherSceneButtons()
    {
        GameObject.Find("Button_Disconnect").GetComponent<Button>().onClick.RemoveAllListeners();
        GameObject.Find("Button_Disconnect").GetComponent<Button>().onClick.AddListener(delegate { DestroyUserData(); });
        GameObject.Find("Button_Disconnect").GetComponent<Button>().onClick.AddListener(NetworkManager.singleton.StopHost);
    }

    void DestroyUserData()
    {
        GameObject ud = GameObject.Find("UserData");
        Destroy(ud.gameObject);
    }
    public void killRoom(int op)
    {
        Packet p = new Packet((int)PacketType.OP_KillRoom, false, Network.player.ipAddress, "");
        string data = JsonUtility.ToJson(p);
        byte[] buffer = Encoding.UTF8.GetBytes(data);
        byte[] buffer2 = new byte[1024];
        s.SendTimeout = 4000;
        s.ReceiveTimeout = 4000;
        try
        {
            s.SendTo(buffer, remoteEpW);
            s.ReceiveFrom(buffer2, ref remoteEpW);
            string data2 = Encoding.UTF8.GetString(buffer2);
            Packet p2 = JsonUtility.FromJson<Packet>(data2);
            if (p2.packetType != (int)PacketType.OP_KillRoomReply)
            {
                throw new Exception();
            }
        }
        catch (Exception)
        {
            if (op == 1)
            {
                killRoom(2);
            }
        }
    }
    public void ghostRoom(string ip)
    {
        Packet p = new Packet((int)PacketType.OP_KillRoom, false, ip, "");
        string data = JsonUtility.ToJson(p);
        byte[] buffer = Encoding.UTF8.GetBytes(data);
        byte[] buffer2 = new byte[1024];
        s.SendTimeout = 4000;
        s.ReceiveTimeout = 4000;
        try
        {
            s.SendTo(buffer, remoteEpW);
            s.ReceiveFrom(buffer2, ref remoteEpW);
            string data2 = Encoding.UTF8.GetString(buffer2);
            Packet p2 = JsonUtility.FromJson<Packet>(data2);
            if (p2.packetType != (int)PacketType.OP_KillRoomReply)
            {
                throw new Exception();
            }
            NetworkManager.singleton.StopClient();
            StartupHost();
        }
        catch (Exception)
        {
            NetworkManager.singleton.StopClient();
            StartupHost();
        }
    }
}