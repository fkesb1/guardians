﻿using UnityEngine;
using System.Collections;
using System.IO;
using LitJson;
using System.Text;
using UnityEngine.UI;

public class userData : MonoBehaviour{
	private int exp=0;
    private string username = "";
	private int maxwave=0;
	private int playcount=0;

    public Text expText;
    public Text nameText;
    public Text maxwaveText;
    public Text playcountText;

    public Text errorText;

    public Canvas nicknameCanvas;
    public InputField nicknameInput;

	public void setExp(int exp){
		this.exp = exp;
    }
    public void setUsername(string username)
    {
        this.username = username;
    }
	public void setMaxwave(int m){
		this.maxwave = m;
	}
	public void setplaycount(int c){
		this.playcount=c;
	}
	public int getExp(){
		return this.exp;
	}
    public string getUsername()
    {
        return this.username;
    }
	public int getMaxWave(){
		return this.maxwave;
	}
	public int getPlayCount(){
		return this.playcount;
	}

    private void setTexts()
    {
        nameText.text = "Name : " + getUsername().ToString();
        if (getMaxWave().Equals(11))
            maxwaveText.text = "Max Wave : Clear";
        else
            maxwaveText.text = "Max Wave : " + getMaxWave().ToString();
        playcountText.text = "Play Count : " + getPlayCount().ToString();
    }

    private string path; 

    private string createJson(string nickname)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(@"{ ""exp"":0,""name"":""");
        sb.Append(nickname);
        sb.Append(@""",""totalkill"":0,""maxkill"":0,""maxwave"":0,""playcount"":0 }");
        string tmp = sb.ToString();
        //string tmp = @"{ ""exp"":0,""name"":" +nickname+ @",""totalkill"":0,""maxkill"":0,""maxwave"":0,""playcount"":0	}";
        return tmp;
    }

    private void createFile(string nickname)
    {
        string tmp = createJson(nickname);
        StreamWriter s = new StreamWriter(path + @"\data.txt");
        s.Write(tmp);
        s.Close();
        JsonData getData = JsonMapper.ToObject(tmp);
        setExp((int)getData["exp"]);
        setUsername((string)getData["name"]);
        setMaxwave((int)getData["maxwave"]);
        setplaycount((int)getData["playcount"]);
    }

    private void readFile()
    {
        StreamReader s = new StreamReader(path + @"\data.txt");
        string tmp = s.ReadLine();
        s.Close();
        JsonData getData = JsonMapper.ToObject(tmp);
        setExp((int)getData["exp"]);
        setUsername((string)getData["name"]);
        setMaxwave((int)getData["maxwave"]);
        setplaycount((int)getData["playcount"]);
    }

    public void loadDataFromGame()
    {
        playcount++;
        int gamewave = GameObject.Find("Spawner_Monster").GetComponent<EnemySpawner>().wave;
        maxwave = (gamewave > maxwave) ? gamewave : maxwave;
    }

    public void save()
    {
        File.Delete(path + @"\data.txt");
        StringBuilder tmp = new StringBuilder(@"{ ""exp"":");
        tmp.Append(getExp());
        tmp.Append(@",""name"":");
        tmp.Append("\"");
        tmp.Append(getUsername());
        tmp.Append("\"");
        tmp.Append(@",""maxwave"":");
        tmp.Append(getMaxWave());
        tmp.Append(@",""playcount"":");
        tmp.Append(getPlayCount());
        tmp.Append(" }");
        StreamWriter s = new StreamWriter(path + @"\data.txt");
        s.Write(tmp);
        s.Close();
    }
    void Start()
    {
        DontDestroyOnLoad(transform.gameObject);
        if (Application.platform == RuntimePlatform.Android)
        {
            path = Application.persistentDataPath;
        }
        else
        {
           path = "./";
        }
        if (!File.Exists(path + @"\data.txt"))
        {
            nicknameCanvas.enabled = true;
        }
        else
        {
            readFile();
            setTexts();
        }
    }

    public void SetNickname()
    {
        if (nicknameInput.text.Length.Equals(0))
        {
            errorText.enabled = true;
        }
        else
        {
            string name = nicknameInput.text;
            createFile(name);
            nicknameCanvas.enabled = false;
            setTexts();
        }
    }
}