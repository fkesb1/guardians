﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionDamage : MonoBehaviour {

	void OnTriggerEnter(Collider col)
    {
        var hit = col.gameObject;
        var hitEnemy = hit.GetComponent<EnemyHP>();

        if (hitEnemy != null)
        {
            hitEnemy.GetDamage(1000);
            Destroy(gameObject);
        }
    }
}
