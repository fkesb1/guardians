﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Text;

public class CastleHp : NetworkBehaviour {
    private Slider castleHpSlider;
    private Text textHp;
    
    [SyncVar]
    public int maxHp;
    [SyncVar]
    public int curHp;

    private StringBuilder sb = new StringBuilder();

    private bool isSave = false;

    private userData data;


    public GameObject boomEffect;

    public Canvas defaultCanvas;
    public Canvas shopCanvas;
    public Canvas optionCanvas;

    public GameObject gameOverCanvas;
    public GameObject victoryCanvas;

    private AudioSource aus;
    public Text waveresultText;
    public Text maxwaveresultText;
    public PlayerInfo teamInfo;

    public EnemySpawner enemySpawner;

    public AudioSource backAudio;
    public AudioClip deathSound;
    public AudioClip victorySound;

    public Canvas RegisterCanvas;

    public GameObject dropShip;

    public AudioSource backMusicObj;

    void Start ()
    {
        aus = GetComponent<AudioSource>();
        castleHpSlider = GameObject.Find("CastleHp").GetComponent<Slider>();
        textHp = GameObject.Find("Text_Hp").GetComponent<Text>();

        textHp.text = SetHpText();
        data = GameObject.Find("UserData").GetComponent<userData>();
    }

    [ClientRpc]
    public void RpcRepairCastleHp()
    {
        castleHpSlider.value += (castleHpSlider.maxValue * 0.5f);
        if (castleHpSlider.value > castleHpSlider.maxValue)
        {
            castleHpSlider.value = castleHpSlider.maxValue;
        }
        textHp.text = SetHpText();
    }
    public string SetHpText()
    {
        maxHp = (int)castleHpSlider.maxValue;
        curHp = (int)castleHpSlider.value;

        sb.Length = 0;

        sb.Append(curHp);
        sb.Append(" / ");
        sb.Append(maxHp);

        return sb.ToString();
    }

    public void GetDamage(int damage)
    {
        if (!isServer) return;
        RpcGetDamage(damage);
    }

    [ClientRpc]
    public void RpcGetDamage(int damage)
    {
        castleHpSlider.value -= damage;
        textHp.text = SetHpText();
        if (castleHpSlider.value <= 0)
        {
            // 죽음 처리
            StartCoroutine("Death", 4.0f);
        }
    }

    IEnumerator Death(float wt)
    {
        if (!isSave)
        {
            isSave = true;

            enemySpawner.StopAllCoroutines();

            data.loadDataFromGame();
            data.save();
            waveresultText.text = enemySpawner.wave.ToString();
            maxwaveresultText.text = "Best wave : " + data.getMaxWave().ToString() + " waves";

            RenderSettings.ambientLight = new Color(0.2117647f, 0.2235294f, 0.254902f);
            backMusicObj.Stop();
            defaultCanvas.enabled = false;
            shopCanvas.enabled = false;
            optionCanvas.enabled = false;
            backAudio.PlayOneShot(deathSound);
            Character.deathcam.SetActive(true);

            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            for (int i = 0; i < players.Length; i++)
            {
                Destroy(players[i].gameObject);
            }

            GameObject[] monsters = GameObject.FindGameObjectsWithTag("Monster");
            for (int i = 0; i < monsters.Length; i++)
            {
                monsters[i].GetComponent<EnemyHP>().GameOver();
            }

            GameObject[] castleBars = GameObject.FindGameObjectsWithTag("CastleBar");
            for (int i = 0; i < castleBars.Length; i++)
            {
                castleBars[i].AddComponent<Rigidbody>();
                castleBars[i].AddComponent<MeshCollider>().convex = true;
                castleBars[i].GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, -100));
            }


            yield return new WaitForSeconds(wt);
            gameOverCanvas.SetActive(true);

            yield return new WaitForSeconds(2f);

            for (int i = 0; i < monsters.Length; i++)
            {
                Destroy(monsters[i].gameObject);
            }

            Destroy(data.gameObject);

            yield return new WaitForSeconds(2f);

            if (isServer)
            {
                RegisterCanvas.enabled = true;
            }

        }
    }

    public IEnumerator Victory(float wt)
    {
        yield return new WaitForSeconds(wt);
        if (!isSave)
        {
            isSave = true;

            enemySpawner.StopAllCoroutines();

            data.loadDataFromGame();
            data.save();

            RenderSettings.ambientLight = new Color(0.2117647f, 0.2235294f, 0.254902f);
            backMusicObj.Stop();
            defaultCanvas.enabled = false;
            shopCanvas.enabled = false;
            optionCanvas.enabled = false;
            backAudio.PlayOneShot(victorySound);

            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            for (int i = 0; i < players.Length; i++)
            {
                players[i].GetComponent<AudioListener>().enabled = false;
                players[i].GetComponent<AudioSource>().enabled = false;
            }

            Instantiate(dropShip);

            yield return new WaitForSeconds(15f);

            victoryCanvas.SetActive(true);

            yield return new WaitForSeconds(1f);

            if (isServer)
            {
                RegisterCanvas.enabled = true;
            }
        }
    }

    public void EndGame()
    {
        NetworkManager.singleton.StopHost();
    }
}
