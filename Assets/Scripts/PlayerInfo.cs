﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class PlayerInfo : NetworkBehaviour {

    [SyncVar]
    public int damage = 20;
    [SyncVar(hook = "MoneyTextUpdate")]
    public int money = 0;
    [SyncVar]
    public float rpm = 0.5f;
    
    public Text moneyText;
    public Text curPower;
    public Text curRpm;
    public Text noMoneyText;

    public Button rpmBtn;

    public CastleHp castleHp;

    private bool isNoMoneyText = false;
    
    public AudioClip purcSuccess;
    public AudioClip purcFail;

    private AudioSource audio;

    // About Rage Skill
    public Text cooltimeText;
    private int coolTime = 90;
    [SyncVar]
    private int timer;
    [SyncVar]
    private bool isCooltime = false;
    

    void Start()
    {
        audio = GetComponent<AudioSource>();
        MoneyTextUpdate(money);
        curPower.text = "(" + damage.ToString() + ")";
        curRpm.text = "(" + rpm.ToString() + ")";

        timer = coolTime;
        cooltimeText.text = null;
    }

    [ClientRpc]
    public void RpcMoneyCheat(int howmuch)
    {
        money = howmuch;
    }

    public void MoneyTextUpdate(int money)
    {
        moneyText.text = money.ToString();
    }

    public void UpgradePower()
    {
        if (money >= 1500)
        {
            if (isServer)
            {
                audio.PlayOneShot(purcSuccess);
                RpcUpgradeDamage();
                money -= 1500;
            }
            else
            {
                audio.PlayOneShot(purcSuccess);
                GameObject me = GameObject.Find(userInfo.playerid);
                me.GetComponent<Character>().CmdUpgradePower();
                me.GetComponent<Character>().CmdUpdateMoney(1500);
            }
        }
        else
        {
            audio.PlayOneShot(purcFail);
            if (isNoMoneyText.Equals(false))
                StartCoroutine("AppearNoMoneyText", 1.0f);
        }
    }

    public void UpgradeRpm()
    {
        if (money >= 2000)
        {
            if (isServer)
            {
                audio.PlayOneShot(purcSuccess);
                RpcUpgradeRpm();
                money -= 2000;
            }
            else
            {
                audio.PlayOneShot(purcSuccess);
                GameObject me = GameObject.Find(userInfo.playerid);
                me.GetComponent<Character>().CmdUpgradeRpm();
                me.GetComponent<Character>().CmdUpdateMoney(2000);
            }
        }
        else
        {
            audio.PlayOneShot(purcFail);
            if (isNoMoneyText.Equals(false))
                StartCoroutine("AppearNoMoneyText", 1.0f);
        }
    }
    public void RepairCastleHpTmp()
    {
        if (money >= 3000)
        {
            if (isServer)
            {
                audio.PlayOneShot(purcSuccess);
                castleHp.RpcRepairCastleHp();
                money -= 3000;
            }
            else
            {
                audio.PlayOneShot(purcSuccess);
                GameObject me = GameObject.Find(userInfo.playerid);
                me.GetComponent<Character>().CmdURepairHp();
                me.GetComponent<Character>().CmdUpdateMoney(3000);
            }
        }
        else
        {
            audio.PlayOneShot(purcFail);
            if (isNoMoneyText.Equals(false))
                StartCoroutine("AppearNoMoneyText", 1.0f);
        }
    }

    IEnumerator AppearNoMoneyText(float wt)
    {
        isNoMoneyText = true;
        noMoneyText.enabled = true;
        yield return new WaitForSeconds(wt);
        noMoneyText.enabled = false;
        isNoMoneyText = false;
    }
    [ClientRpc]
    public void RpcUpgradeDamage()
    {
        damage += 5;
        curPower.text = "(" + damage.ToString() + ")";
    }
    [ClientRpc]
    public void RpcUpgradeRpm()
    {
        rpm -= 0.05f;
        rpm = Mathf.Round(rpm / 0.01f) * 0.01f;

        if (rpm >= 0.14 && rpm <= 0.16)
        {
            rpmBtn.interactable = false;
            rpmBtn.image.color = Color.red;
        }

        curRpm.text = "(" + rpm.ToString() + ")";
    }
    [ClientRpc]
    public void RpcUpdateMoney(int cost)
    {
        money -= cost;
    }

    [ClientRpc]
    public void RpcRage()
    {
        if (isCooltime) return;
        isCooltime = true;
        StartCoroutine("RageTime", 10f);
        StartCoroutine("CoolTime", 0f);
    }

    IEnumerator RageTime(float wt)
    {
        RenderSettings.ambientLight = Color.red;
        damage *= 2;
        rpm -= 0.05f;
        yield return new WaitForSeconds(wt);
        RenderSettings.ambientLight = new Color(0.2117647f, 0.2235294f, 0.254902f);
        damage /= 2;
        rpm += 0.05f;
    }

    IEnumerator CoolTime(int wt)
    {
        if (isCooltime)
        {
            //isCooltime = true;
            //RpcCooltimeBtn();
            //yield return new WaitForSeconds(wt);
            //isCooltime = false;
            //RpcReadyBtn();
            yield return new WaitForSeconds(wt);

            CooltimeBtn(timer.ToString());
            if (timer.Equals(0))
            {
                isCooltime = false;
                ReadyBtn();
                timer = coolTime;
            }

            timer--;
            StartCoroutine("CoolTime", 1.0f);
        }
    }
    
    void CooltimeBtn(string time)
    {
        cooltimeText.text = time;
    }
    
    void ReadyBtn()
    {
        cooltimeText.text = null;
    }
}
