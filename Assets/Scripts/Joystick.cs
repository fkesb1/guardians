﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Joystick : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    public Character player;

    public Transform stick;

    private Vector3 axis;

    private float radius;
    private Vector3 defaultCenter;

    public string playerid;

    void Awake()
    {
        radius = GetComponent<RectTransform>().sizeDelta.y * 0.5f;
        defaultCenter = stick.position;
    }

    void Start()
    {
        defaultCenter = transform.position;
        StartCoroutine("SetPlayer", 1f);
        
    }

    IEnumerator SetPlayer(float wt)
    {
        yield return new WaitForSeconds(wt);
        player = GameObject.Find("Player " + playerid).GetComponent<Character>();
    }

    void Movement(Vector3 touchPos)
    {
        axis = (touchPos - defaultCenter).normalized;
        float fDistance = Vector3.Distance(touchPos, defaultCenter);
        if (fDistance > radius)
            stick.transform.position = defaultCenter + axis * radius;
        else
            stick.transform.position = defaultCenter + axis * fDistance;
    }

    public void OnPointerDown(PointerEventData data)
    {
        Movement(data.position);
        //player.Move(axis, true);
        player.JoystickMoveCtrl(axis, false);
    }

    public void OnPointerUp(PointerEventData data)
    {
        axis = Vector3.zero;
        stick.transform.position = defaultCenter;
        player.JoystickMoveCtrl(axis, true);
    }

    public void OnDrag(PointerEventData data)
    {
        Movement(data.position);
        player.JoystickMoveCtrl(axis, false);
    }

}
