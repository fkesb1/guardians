﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Text;

public class EnemySpawner : NetworkBehaviour {
	public GameObject ZombiePrefab;
    public GameObject GhostPrefab;
    public GameObject SkeletonPrefab;
    public GameObject BossPrefab;

    [SyncVar]
    public int time = 60;
	private bool isWave = true;

    [SyncVar]
    public int wave = 1;

    
    public Text textWave;
    public Text textTimer;
    

    private StringBuilder sb = new StringBuilder();

    private AudioSource audio;
    public AudioClip waveStartaudio;
    public AudioClip bossAppearSound;

    public CastleHp castle;

    private GameObject bossHpSlider;

	void Start()
    {
        bossHpSlider = GameObject.Find("BossHp");
        bossHpSlider.SetActive(false);
        audio = GetComponent<AudioSource>();
        textWave.text = "Waiting";
        textTimer.text = "...";
    }

    public void HostGameStart()
    {
        GameObject.Find("NetworkManager").GetComponent<NetworkManager_Custom>().killRoom(1);
        audio.PlayOneShot(waveStartaudio);
        if (!isServer) return;
        StartCoroutine("create", 0f);
        StartCoroutine("WaveTimer", 0f);
    }

    IEnumerator WaveTimer(float wt)
    {
        if (wave.Equals(10)) yield break;

        yield return new WaitForSeconds(wt);
        RpcUpdateTime(time.ToString());
        if (time.Equals(30))
        {
            isWave = false;
        }

        if (time.Equals(0))
        {
            NextWave();
        }

        if (!isWave)
        {
            GameObject mon = GameObject.FindWithTag("Monster");
            if (mon == null) NextWave();
        }
        sb.Length = 0;
        sb.Append("Wave ");
        sb.Append(wave);
        RpcUpdateWave(sb.ToString());

        time--;

        StartCoroutine("WaveTimer", 1.0f);
    }

    private void NextWave()
    {
        wave++;
        if(wave.Equals(10))
        {
            RpcUpdateWave("Wave 10");
            RpcUpdateTime("Boss");
            RpcBossWave();
        }
        else
        {
            time = 60;
            RpcUpdateWave("Wave " + wave);
            RpcUpdateTime(time.ToString());
            isWave = true;
            audio.PlayOneShot(waveStartaudio);
            StartCoroutine("create", 1f);
        }
    }

    public void CheatWave(int go)
    {
        if (!isServer) return;
        StopAllCoroutines();
        GameObject[] monsters = GameObject.FindGameObjectsWithTag("Monster");
        for (int i = 0; i < monsters.Length; i++)
        {
            Destroy(monsters[i].gameObject);
        }
        wave = go;
        if (wave.Equals(10))
        {
            RpcUpdateWave("Wave 10");
            RpcUpdateTime("Boss");
            RpcBossWave();
        }
        else
        {
            time = 60;
            RpcUpdateWave("Wave " + wave);
            RpcUpdateTime(time.ToString());
            isWave = true;
            audio.PlayOneShot(waveStartaudio);
            StartCoroutine("create", 1f);
            StartCoroutine("WaveTimer", 0f);
        }
    }

    [ClientRpc]
    private void RpcUpdateTime(string dialog)
    {
        textTimer.text = dialog;
    }

    [ClientRpc]
    private void RpcUpdateWave(string dialog)
    {
        textWave.text = dialog;
    }

    [ClientRpc]
    private void RpcBossWave()
    {
        audio.PlayOneShot(bossAppearSound);
        bossHpSlider.SetActive(true);
        if (!isServer) return;
        BossAppear();
    }

	IEnumerator create(float wt)
    {
        yield return new WaitForSeconds (wt);

        if (isWave)
        {
            
            int monsterSet = Random.Range(0, 3);

            if (monsterSet.Equals(0))
            {
                ZombieSet();
            }
            else if(monsterSet.Equals(1))
            {
                SkeletonSet();
            }
            else if(monsterSet.Equals(2))
            {
                GhostSet();
            }
            StartCoroutine("create", 3.0f);
        }
	}

    void ZombieSet()
    {
        for (int i = 0; i < 4; i++)
        {
            Vector3 spawnPoint = new Vector3(Random.Range(77, 122), 3.6f, 400f);
            GameObject zombie = (GameObject)Instantiate(ZombiePrefab, spawnPoint, Quaternion.Euler(0, 180, 0));
            NetworkServer.Spawn(zombie);
        }
    }

    void GhostSet()
    {
        for (int i = 0; i < 2; i++)
        {
            Vector3 spawnPoint = new Vector3(Random.Range(77, 122), 3f, 400f);
            GameObject ghost = (GameObject)Instantiate(GhostPrefab, spawnPoint, Quaternion.Euler(0, 180, 0));
            NetworkServer.Spawn(ghost);
        }
    }

    void SkeletonSet()
    {
        for (int i = 0; i < 2; i++)
        {
            Vector3 spawnPoint = new Vector3(Random.Range(77, 122), -0.17f, 400f);
            GameObject skel = (GameObject)Instantiate(SkeletonPrefab, spawnPoint, Quaternion.Euler(0, 180, 0));
            NetworkServer.Spawn(skel);
        }
    }

    void BossAppear()
    {
        Vector3 spawnpoint = new Vector3(99, 0.44f, 400);
        GameObject boss = Instantiate(BossPrefab, spawnpoint, Quaternion.Euler(0, 180, 0));
        NetworkServer.Spawn(boss);
    }

    [ClientRpc]
    public void RpcVictory()
    {
        wave++;
        castle.StartCoroutine("Victory", 5f);
    }
}
