﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class MonZombie : NetworkBehaviour {
    private CastleHp castlehp;

    private AudioSource audio;
    public AudioClip atkSound;

    private Animator anim;
    private float speed = 20.0f;
    private int damage = 2;

    [SyncVar]
    public bool isDead = false;

	void Start ()
    {
        audio = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        castlehp = GameObject.Find("Castle_Final").GetComponent<CastleHp>();
	}
    

    void Update()
    {
        if (!isDead)
        {
            if (transform.position.z > 50)
            {
                if (anim.GetBool("isAttack").Equals(true))
                    anim.SetBool("isAttack", false);
                transform.Translate(Vector3.forward * speed * Time.deltaTime);
            }
            else
            {
                if (anim.GetBool("isAttack").Equals(false))
                    anim.SetBool("isAttack", true);
            }
        }
    }
    
    void DamageCastle()
    {
        audio.PlayOneShot(atkSound);
        castlehp.GetDamage(damage);
    }
}
