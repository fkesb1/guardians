﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Option_Toggle : MonoBehaviour {

    public Text toggleText;

    public void ChangeText()
    {
        if (!GetComponent<Toggle>().isOn)
        {
            toggleText.text = "OFF";
            toggleText.color = Color.gray;
        }
        else
        {
            toggleText.text = "ON";
            toggleText.color = Color.yellow;
        }
    }
}
