﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class MonSkeleton : NetworkBehaviour {

    private CastleHp castlehp;

    private AudioSource audio;
    public AudioClip atkSound;

    private Animator anim;
    private float speed = 25;
    private int damage = 5;

    [SyncVar]
    public bool isDead = false;

    void Start()
    {
        castlehp = GameObject.Find("Castle_Final").GetComponent<CastleHp>();

        audio = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
    }
    
    void Update()
    {
        if (!isDead)
        {
            if (transform.position.z > 52)
            {
                if (anim.GetBool("isAttack").Equals(true))
                    anim.SetBool("isAttack", false);
                transform.Translate(Vector3.forward * speed * Time.deltaTime);
            }
            else
            {
                if (anim.GetBool("isAttack").Equals(false))
                    anim.SetBool("isAttack", true);
            }
        }
    }
    void DamageCastle()
    {
        audio.PlayOneShot(atkSound);
        castlehp.GetDamage(damage);
    }

}
