﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Character : NetworkBehaviour
{

    // 플레이어 애니메이터
    public Animator anim;
    // 플레이어 카메라
    public Camera cam;
    // 플레이어 머리
    public GameObject head;
    public GameObject arm;

    // 총알
    public GameObject bulletPrefab;
    // 총알 발사위치
    public GameObject Firepos;
    public GameObject shootEffect;

    private Slider sensivitySlider;

    private AudioSource aus;
    public AudioClip shootSound;

    
    private Joystick joy;
    private FireBtn fireBtn;
    private MissileBtn missileBtn;
    private RageBtn rageBtn;
    private PlayerInfo teamInfo;


    // 캐릭터 이동속도
    private float mSpeed = 10f;

    // x, y축 회전 감도
    private float sensivityX;
    private float sensivityY;

    // y축 회전 최대, 최소치
    private int maxY = 45;
    private int minY = -45;

    // y축 회전 저장 변수
    private float rotateY = 0;

    // Axis 키 입력 변수
    private float hor;
    private float ver;

    // 조이스틱 포지션 벡터 함수
    private Vector3 JoystickPosition;


    // 화면 이동 터치가 먼저인지 확인하는 변수
    public bool isTouchScreenFirst;

    // 위 변수 임시 저장
    private bool tmpTouchFirst;


        

    private string id;

    public static GameObject deathcam;

    public void changeSensivity()
    {
        sensivityX = sensivitySlider.value;
        sensivityY = sensivitySlider.value;
    }

    public override void OnStartLocalPlayer()
    {
        if(Screen.width.Equals(2560) && Screen.height.Equals(1440))
        {
            sensivityX = 0.5f;
            sensivityY = 0.5f;
        }
        else if(Screen.width.Equals(1920) && Screen.height.Equals(1080))
        {
            sensivityX = 1f;
            sensivityY = 1f;
        }
        else if(Screen.width.Equals(1280) && Screen.height.Equals(720))
        {
            sensivityX = 1.5f;
            sensivityY = 1.5f;
        }
        else
        {
            sensivityX = 0.5f;
            sensivityY = 0.5f;
        }

        deathcam = GameObject.Find("Camera");
        deathcam.SetActive(false);
        cam.enabled = true;
        GetComponent<AudioListener>().enabled = true;
        sensivitySlider = GameObject.Find("Slider_Sensivity").GetComponent<Slider>();
        sensivitySlider.onValueChanged.AddListener(delegate { changeSensivity(); });
        id = GetComponent<NetworkIdentity>().netId.ToString();
        teamInfo = GameObject.Find("TeamInfo").GetComponent<PlayerInfo>();
        aus = GetComponent<AudioSource>();
        joy.playerid = id;
        fireBtn.playerid = id;
        missileBtn.playerid = id;
        rageBtn.playerid = id;
    }

    void Awake()
    {
        joy = GameObject.Find("Joypad").GetComponent<Joystick>();
        fireBtn = GameObject.Find("Button_Attack").GetComponent<FireBtn>();
        missileBtn = GameObject.Find("Button_MissileSkill").GetComponent<MissileBtn>();
        rageBtn = GameObject.Find("Button_RageSkill").GetComponent<RageBtn>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (!isLocalPlayer) return;

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (Input.touchCount == 1 || tmpTouchFirst)
            {
                if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                {
                    isTouchScreenFirst = true;
                    tmpTouchFirst = isTouchScreenFirst;
                }
            }

            if (isTouchScreenFirst)
            {
                touch = Input.GetTouch(0);
            }
            else
            {
                touch = Input.GetTouch(1);
            }


            if (touch.phase == TouchPhase.Moved)
            {
                if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                {
                    transform.Rotate(0, touch.deltaPosition.x * sensivityX, 0);
                    rotateY += touch.deltaPosition.y * sensivityY;
                    rotateY = Mathf.Clamp(rotateY, minY, maxY);
                    arm.transform.localEulerAngles = new Vector3(-rotateY, arm.transform.localEulerAngles.y, 0);
                    head.transform.localEulerAngles = new Vector3(-rotateY/2, head.transform.localEulerAngles.y, 0);
                }
            }

            if (touch.phase == TouchPhase.Ended)
            {
                isTouchScreenFirst = false;
            }
        }

        if (Input.touchCount == 0)
            tmpTouchFirst = false;

    }

    [Command]
    public void CmdFire(int damage)
    {
        anim.SetBool("isShoot", true);
        GameObject bullet = (GameObject)Instantiate(bulletPrefab, Firepos.transform.position, cam.transform.rotation);
        GameObject shootEffectq = (GameObject)Instantiate(shootEffect, Firepos.transform.position, cam.transform.rotation);
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 500;
        bullet.GetComponent<Bullet>().damage = damage;
        NetworkServer.Spawn(bullet);
        NetworkServer.Spawn(shootEffectq);
        Destroy(shootEffectq, 0.5f);
        Destroy(bullet, 3.0f);
    }
    
    public void ShootingSound()
    {
        aus.PlayOneShot(shootSound);
    }

    public void JoystickMoveCtrl(Vector3 position, bool endTouch)
    {
        if (!isLocalPlayer) return;

        if (!endTouch)
            anim.SetBool("isRun", true);
        else
            anim.SetBool("isRun", false);

        StopCoroutine("JoystickActive");
        StartCoroutine("JoystickActive", position);
    }

    IEnumerator JoystickActive(Vector3 position)
    {
        while (true)
        {
            transform.Translate(new Vector3(position.x, 0, position.y) * mSpeed * Time.deltaTime);
            yield return null;
        }
    }



    [Command]
    public void CmdUpgradePower()
    {
        GameObject.Find("TeamInfo").GetComponent<PlayerInfo>().RpcUpgradeDamage();
    }
    [Command]
    public void CmdUpgradeRpm()
    {
        GameObject.Find("TeamInfo").GetComponent<PlayerInfo>().RpcUpgradeRpm();
    }
    [Command]
    public void CmdURepairHp()
    {
        GameObject.Find("Castle_Final").GetComponent<CastleHp>().RpcRepairCastleHp();
    }
    [Command]
    public void CmdUpdateMoney(int money)
    {
        GameObject.Find("TeamInfo").GetComponent<PlayerInfo>().RpcUpdateMoney(money);
    }

    // Skills
    [Command]
    public void CmdMissileLaunch()
    {
        GameObject.Find("HellFireMissileLaunch").GetComponent<MissileLaunch>().LaunchMissile();
    }
    [Command]
    public void CmdRage()
    {
        GameObject.Find("TeamInfo").GetComponent<PlayerInfo>().RpcRage();
        aus.Play();
    }
    void OnCollisionEnter(Collision col)
    {
        if(col.transform.name.Equals("Portal_R"))
        {
            transform.position = new Vector3(59, 28.71f, 58);
            transform.rotation = new Quaternion(0, 0, 0, 0);
        }
        if (col.transform.name.Equals("Portal_L"))
        {
            transform.position = new Vector3(145, 28.71f, 58);
            transform.rotation = new Quaternion(0, 0, 0, 0);
        }
    }
}
